//
//  AlertManager.swift
//  FacebookPhoto
//
//  Created by Anna on 04.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import UIKit
protocol AlertManager {
    
}

extension AlertManager {
    
    func showAlert(_ message: String, presenter: UIViewController) {
        let alert = UIAlertController(title: "Warning!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        presenter.present(alert, animated: true, completion: nil)
    }
}
