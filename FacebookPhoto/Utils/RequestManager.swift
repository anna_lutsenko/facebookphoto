
//
//  RequestManager.swift
//  FacebookPhoto
//
//  Created by Anna on 04.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import AlamofireImage

enum RequestManagerError: Error {
    case unknown
}

class RequestManager {
    static let sharedInstance = RequestManager()
    
    func getUserInfo(with token: String, succeed: @escaping ( (User) -> () ), failure: @escaping ( (Error) -> () )) {
        
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"], httpMethod: "GET")
        
        _ = graphRequest?.start(completionHandler: { (_, result, error) in
            if let err = error {
                failure(err)
                return
            }
            
            guard let data = result as? [String : Any] else {
                failure(RequestManagerError.unknown)
                return
            }
            
            guard let id : String = data["id"] as? String,
                let name : String = data["name"] as? String else { return }
            
            let user = User(name: name, id: id)
            succeed(user)
        })
    }
    
    
    func getAlbums(succeed: @escaping (([Album])->()), failure: @escaping ((Error)->())) {
        guard let id = UserManager.shared.currentUser?.id else { return }
        let path = "/\(id)/albums"
        
        let request = FBSDKGraphRequest(graphPath: path, parameters: nil, httpMethod: "GET")
        _ = request?.start(completionHandler: { (_, result, error) in
            if let error = error {
                failure(error)
                return
            } else {
                guard let res = result as? [String : Any],
                       let data = res["data"] as? [[String : Any]] else {
                    failure(RequestManagerError.unknown)
                    return
                }
                var albums = [Album]()
                for dict in data {
                    guard let name = dict["name"] as? String,
                        let id = dict["id"] as? String else { return }
                    
                    albums.append(Album(id: id, name: name))
                }
                succeed(albums)
            }
        })
    }
 
    func getCoverPhoto(id: String, completion: @escaping ((UIImage?) -> ()) ) {
        let request = FBSDKGraphRequest(graphPath: "/\(id)?fields=picture", parameters: nil, httpMethod: "GET")
        
        _ = request?.start(completionHandler: { (connection, result, error) in
            if error == nil {
                guard let data = result as? [String:Any],
                    let picture = data["picture"] as? [String:Any] else { return }
                self.parseJSON(from: picture) { img in
                    completion(img)
                }
            }
        })
    }
    
    func parseJSON(from dict: [String: Any], succeess:@escaping (UIImage?)->()) {
        guard let imgData = dict["data"] as? [String:Any],
            let strURL = imgData["url"] as? String else { return }
        
        getImageFrom(url: strURL) { img in
            succeess(img)
        }
    }
    
    func getImageFrom(url: String?, success: @escaping (UIImage?)->()) {
        guard let url = url else {
            success(nil)
            return
        }
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                success(image)
            } else {
                success(nil)
            }
        }
    }
    
    func getPhotos(id: String, succeed: @escaping (([Photo])->()), failure: @escaping ((Error)->())) {
        let request = FBSDKGraphRequest(graphPath: "/\(id)/photos", parameters: nil, httpMethod: "GET")
        
        _ = request?.start(completionHandler: { (_, result, error) in
            if let err = error {
                failure(err)
            } else {
                guard let res = result as? [String: Any],
                    let data = res["data"] as? [[String: Any]] else { return }
                var photos = [Photo]()
                for img in data {
                    guard let id = img["id"] as? String else { return }
                    photos.append(Photo(id: id))
                }
                succeed(photos)
            }
        })
    }
    
    func getImages(id: String, succeed: @escaping ((Photo)->()), failure: @escaping ((Error)->())) {
        
         let request = FBSDKGraphRequest(graphPath: "/\(id)/?fields=images", parameters: nil, httpMethod: "GET")
    
        var photos = Photo(id: id)
        _ = request?.start(completionHandler: { (_, result, error) in
            if let err = error {
                failure(err)
            } else {
                guard let res = result as? [String: Any],
                    let images = res["images"] as? [[String: Any]] else { return }
             
                /* Here I try to find the biggest and the least images from array of images. As Facebook return array of images with different sizes. I decided to use the smallest image for thumbnail and the biggest for full image.
 */
                
                var minTuple = (images.first?["width"] as? Int ?? 0, images.first?["source"] as? String)
                var maxTuple = (images.first?["width"] as? Int ?? 0, images.first?["source"] as? String)
                
                for img in images {
                guard let source = img["source"] as? String,
                        let width = img["width"] as? Int else { return }
                    
                    if width < minTuple.0 {
                        minTuple.0 = width
                        minTuple.1 = source
                    }
                    
                    if width > maxTuple.0 {
                        maxTuple.0 = width
                        maxTuple.1 = source
                    }
                }
                photos = Photo(id: id, litleImgURL: minTuple.1, bigImgURL: maxTuple.1)
                succeed(photos)
            }
        })
    }
    
}
