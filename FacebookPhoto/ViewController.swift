//
//  ViewController.swift
//  FacebookPhoto
//
//  Created by Anna on 03.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SVProgressHUD

class ViewController: UIViewController, AlertManager {
    
    @IBOutlet weak var albumsBtn: UIBarButtonItem!
    let readPermission = ["public_profile", "email", "user_friends", "user_photos"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let loginBtn = FBSDKLoginButton()
        loginBtn.center = self.view.center
        loginBtn.readPermissions = readPermission
        loginBtn.delegate = self
        self.view.addSubview(loginBtn)
        
        getUserDataAndNavigateToAlbum()
    }
    
    @IBAction func openAlbums(_ sender: UIBarButtonItem) {
        guard UserManager.shared.currentUser != nil else { return }
        self.openListAlbum()
    }
    
    func openListAlbum() {
        let vc = AlbumsTableViewController.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getUserDataAndNavigateToAlbum() {
        SVProgressHUD.show()
        UserManager.shared.getUserData(succeed: { _ in
            SVProgressHUD.dismiss()
            self.albumsBtn.isEnabled = true
            self.openListAlbum()
        }) { (error) in
            SVProgressHUD.dismiss()
            self.albumsBtn.isEnabled = false
            if error as? UserError != UserError.unauthorized {
                self.showAlert(error.localizedDescription, presenter: self)
            }
        }
    }

}

extension ViewController : FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if !result.isCancelled {
            self.albumsBtn.isEnabled = true
            getUserDataAndNavigateToAlbum()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        self.albumsBtn.isEnabled = false 
        UserManager.shared.logout()
    }
    
}
