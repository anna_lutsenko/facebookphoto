//
//  User.swift
//  FacebookPhoto
//
//  Created by Anna on 04.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation

struct User {
    var name: String
    var id : String
}
