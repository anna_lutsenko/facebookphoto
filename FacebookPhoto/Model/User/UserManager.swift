//
//  UserManager.swift
//  FacebookPhoto
//
//  Created by Anna on 04.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import FBSDKLoginKit

enum UserError : Error {
    case unauthorized
}

class UserManager {
    static let shared = UserManager()
    private (set) var currentUser: User?
    
    func getUserData (succeed: @escaping ( (User) -> () ), failure: @escaping ( (Error) -> () ) ) {
        if let token = FBSDKAccessToken.current() {
            RequestManager.sharedInstance.getUserInfo(with: token.tokenString, succeed: { (user) in
                self.currentUser = user
                DispatchQueue.main.async {
                    succeed(user)
                }
            }, failure: { error in
                DispatchQueue.main.async {
                    failure(error)
                }
            })
        } else {
            failure(UserError.unauthorized)
        }
    }
    
    func logout() {
        currentUser = nil 
    }
}
