//
//  Album.swift
//  FacebookPhoto
//
//  Created by Anna on 04.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import UIKit

struct Album {
    var id: String
    var name: String
    var image: UIImage?
    
    init(id: String, name: String, image: UIImage? = nil) {
        self.id = id
        self.name = name
        self.image = image
    }
}
