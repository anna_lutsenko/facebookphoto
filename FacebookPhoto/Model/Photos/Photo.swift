//
//  Photo.swift
//  FacebookPhoto
//
//  Created by Anna on 04.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import UIKit

struct Photo {
    var id: String
    var litleImgURL: String?
    var bigImgURL: String?
    
    var imageURL : URL? {
        guard let str = litleImgURL,
            let url = URL(string: str) else { return nil }
        return  url
    }
    
    init(id: String, litleImgURL: String? = nil, bigImgURL: String? = nil) {
        self.id = id
        self.litleImgURL = litleImgURL
        self.bigImgURL = bigImgURL
    }
}
