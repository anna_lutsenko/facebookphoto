//
//  PhotosCollectionViewController.swift
//  FacebookPhoto
//
//  Created by Anna on 05.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit
import GSImageViewerController
import SVProgressHUD

private let reuseIdentifier = "reuseIdentifier"

class PhotosCollectionViewController: UICollectionViewController, AlertManager {
    
    let requestManager = RequestManager.sharedInstance
    var album: Album!
    var albumID: String = ""
    var albumName: String = ""
    var photos = [Photo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        requestManager.getPhotos(id: albumID, succeed: { photos in
            self.photos = photos
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.collectionView?.reloadData()
            }
        }) { error in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.showAlert(error.localizedDescription, presenter: self)
            }
        }

        self.navigationItem.title = albumName
    }

    static func instanceVC() -> PhotosCollectionViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PhotosCollectionViewController") as! PhotosCollectionViewController
        return vc
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return photos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let bigImgUrl = photos[indexPath.row].bigImgURL else {return}
        
        requestManager.getImageFrom(url: bigImgUrl) { img in
            guard let img = img else { return }
            let imageInfo = GSImageInfo(image: img, imageMode: .aspectFit)
            let imageViewer = GSImageViewerController(imageInfo: imageInfo)
            self.navigationController?.pushViewController(imageViewer, animated: true)
        }
    }

}

extension PhotosCollectionViewController {
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let photo = photos[indexPath.row]

        requestManager.getImages(id: photo.id, succeed: { photo in
            DispatchQueue.main.async {
                let cell = collectionView.cellForItem(at: indexPath) as? PhotoCollectionViewCell
                self.photos[indexPath.row].litleImgURL = photo.litleImgURL
                self.photos[indexPath.row].bigImgURL = photo.bigImgURL
                guard let str = photo.imageURL else { return }
                cell?.image.af_setImage(withURL: str)
            }
        }, failure: { (error) in
           let cell = collectionView.cellForItem(at: indexPath) as? PhotoCollectionViewCell
            cell?.image.image = #imageLiteral(resourceName: "thumbnail")
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as? PhotoCollectionViewCell
        cell?.image.image = nil
    }
    
}
