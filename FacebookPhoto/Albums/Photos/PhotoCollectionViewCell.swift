//
//  PhotoCollectionViewCell.swift
//  FacebookPhoto
//
//  Created by Anna on 05.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
}
