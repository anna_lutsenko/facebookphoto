//
//  AlbumsTableViewController.swift
//  FacebookPhoto
//
//  Created by Anna on 04.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SVProgressHUD

class AlbumsTableViewController: UITableViewController, AlertManager {
    let requestManager = RequestManager.sharedInstance
    var albums: [Album] = []
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        requestManager.getAlbums(succeed: { (albums) in
            self.albums = albums
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.tableView.reloadData()
            }
        }) { (error) in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.showAlert(error.localizedDescription, presenter: self)
            }
        }
        
        self.navigationItem.title = UserManager.shared.currentUser?.name
    }
    
    static func instanceVC() -> AlbumsTableViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AlbumsTableViewController") as! AlbumsTableViewController
        return vc
    }

    // MARK: - UITableViewDataSource

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let album = albums[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? AlbumsTableViewCell ?? AlbumsTableViewCell(style: .default, reuseIdentifier: "reuseIdentifier")
        
        cell.labelTitle.text = album.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let album = albums[indexPath.row]
        
        if album.image == nil {
            requestManager.getCoverPhoto(id: album.id) { img in
                DispatchQueue.main.async {
                    self.albums[indexPath.row].image = img
                    let cell = tableView.cellForRow(at: indexPath) as? AlbumsTableViewCell
                    cell?.imageCover.image = img
                }
            }
        } else {
            (cell as? AlbumsTableViewCell)?.imageCover.image = album.image
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = PhotosCollectionViewController.instanceVC()
        vc.albumID = albums[indexPath.row].id
        vc.albumName = albums[indexPath.row].name
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
